import { mapState, mapActions } from 'vuex';

const jMixNotificationScreen = {
  computed: {
    ...mapState('jStoreNotificationScreen', ['info_screen', 'problem_screen', 'success_screen', 'loading_screen']),
    ...mapState('jStoreNotificationScreen', ['info_message', 'problem_message', 'problem_code', 'success_message', 'loading_message']),
  },
  methods: {
    ...mapActions('jStoreNotificationScreen', ['toggleInfo', 'toggleProblem', 'toggleSuccess', 'toggleLoading', 'hideLoading', 'showLoading']),
  },
  watch: {
    // loading when move to another path
    $route() {
      this.$store.dispatch('jStoreLogin/getProfileAndRole');
    },
    // trigger info_screen
    info_screen() {
      const info = this.$refs.infoScreen;
      if (this.info_screen === true) {
        info.toggleInfoScreen();
        this.toggleInfo();
      }
    },
    // trigger problem_screen
    problem_screen() {
      const problem = this.$refs.problemScreen;
      if (this.problem_screen === true) {
        problem.toggleProblemScreen();
        this.toggleProblem();
      }
    },
    // trigger success_screen
    success_screen() {
      const success = this.$refs.successScreen;
      if (this.success_screen === true) {
        success.toggleSuccessScreen();
        this.toggleSuccess();
      }
    },
    // tigger loading_screen
    loading_screen() {
      const loading = this.$refs.loadingScreen;
      if (this.loading_screen === true) {
        loading.showLoadingScreen();
      } else {
        loading.closeLoadingScreen();
      }
    },
  },
};

export default jMixNotificationScreen;
