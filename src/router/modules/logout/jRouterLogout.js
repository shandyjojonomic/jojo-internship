import jLogin from '@/views/login/jLogin';

export default [
  {
    path: '/logout',
    name: 'jLogout',
    component: jLogin,
    beforeEnter(to, from, next) {
      localStorage.clear();
      if (process.env.JOJOPRO_URL) {
        window.location = `${process.env.JOJOPRO_URL}logout`;
      } else {
        next('/');
      }
    },
  },
];
