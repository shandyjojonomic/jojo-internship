import jLogin from '@/views/login/jLogin';
import Vue from 'vue';

export default [
  {
    path: '/',
    name: 'jLogin',
    component: jLogin,
    beforeEnter(to, from, next) {
      if (Vue.ls.get('Token')) {
        next('/home');
      } else {
        next();
      }
    },
    meta: {
      title: 'Login',
    },
  },
];
