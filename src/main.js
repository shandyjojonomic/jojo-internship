/* eslint-disable import/no-unresolved,import/first */
import * as all from '@jojocoders/jojonomic-ui';
import { layout } from 'vue-extend-layout';
import Vue from 'vue';
import stores from './stores/index';
import Storage from 'vue-ls';
import router from './router';

// axios global
window.axios = require('axios');

// vendor
const keys = Object.keys(all);
keys.forEach((data) => {
  if (Array.isArray(all[data])) {
    Vue.use(...all[data]);
  } else {
    Vue.use(all[data]);
  }
});

Vue.use(Storage);

// import this project scss
import './sass/main.scss';

// register global component here
// ex: Vue.component('header', Header)

Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store: stores,
  ...layout,
});
