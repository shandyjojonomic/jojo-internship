import jApi from './jApi';

export default {
  getProfile() {
    const api = jApi.generateApi();
    return api.get('user/profile')
      .then(res => res.data.data);
  },
};
