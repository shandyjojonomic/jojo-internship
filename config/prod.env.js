'use strict'
// write env here for production

const dotenv = require('dotenv');

dotenv.config({ path: `${__dirname}/../.env` });

module.exports = {
  NODE_ENV: JSON.stringify(process.env.NODE_ENV),
  GATE_URL: JSON.stringify(process.env.GATE_URL),
  COMPANY: JSON.stringify(process.env.COMPANY),
  JOJOPRO_URL: JSON.stringify(process.env.JOJOPRO_URL),
  SENTRY_DSN: JSON.stringify(process.env.SENTRY_DSN),
}
